<Card style={{ width: '100' }}>
  	<Card.Body>
    	<Card.Title>Sample Course</Card.Title>
    	<Card.Subtitle className="mb-2 text-muted">Description:</Card.Subtitle>
    	<Card.Text>
      	This is a sample course offering.
      	</Card.Text>
      	<Card.Text>
      	Price:
      	Php 40,000
    	</Card.Text>
    	<Button variant = "primary">Enroll</Button>
  	</Card.Body>
</Card>